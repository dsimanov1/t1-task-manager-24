package ru.t1.simanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.model.User;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Change user password.";

    @NotNull
    public static final String NAME = "user-change-password";

    @Override
    public void execute() {
        @NotNull final User user = getAuthService().getUser();
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        getUserService().setPassword(user.getId(), newPassword);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
