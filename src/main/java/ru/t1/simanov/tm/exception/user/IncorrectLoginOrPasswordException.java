package ru.t1.simanov.tm.exception.user;

public final class IncorrectLoginOrPasswordException extends AbstractUserException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Incorrect Login or Password entered. Please try again...");
    }

}
